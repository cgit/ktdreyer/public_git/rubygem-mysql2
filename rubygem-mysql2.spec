# Generated from mysql2-0.3.11.gem by gem2rpm -*- rpm-spec -*-
%global gem_name mysql2
%if 0%{?el6}
%global rubyabi 1.8
%else
%global rubyabi 1.9.1
%endif

Summary: A simple, fast Mysql library for Ruby, binding to libmysql
Name: rubygem-%{gem_name}
Version: 0.3.11
Release: 3%{?dist}
Group: Development/Languages
License: MIT
URL: http://github.com/brianmario/mysql2
Source0: http://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems)
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby-devel
%if 0%{?fedora}
BuildRequires: rubygems-devel
%else
BuildRequires: ruby(rubygems)
%endif
BuildRequires: mysql-devel
BuildRequires: mysql-server
BuildRequires: rubygem(eventmachine)
BuildRequires: rubygem(rspec)
Provides: rubygem(%{gem_name}) = %{version}

# macros for RHEL6 compatibility:
%{!?gem_dir: %global gem_dir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)}
%{!?gem_instdir: %global gem_instdir %{gem_dir}/gems/%{gem_name}-%{version}}
%{!?gem_libdir: %global gem_libdir %{gem_instdir}/lib}
%{!?gem_cache: %global gem_cache %{gem_dir}/cache/%{gem_name}-%{version}.gem}
%{!?gem_spec: %global gem_spec %{gem_dir}/specifications/%{gem_name}-%{version}.gemspec}
%{!?gem_docdir: %global gem_docdir %{gem_dir}/doc/%{gem_name}-%{version}}
%{!?gem_extdir: %global gem_extdir %{_libdir}/gems/exts/%{gem_name}-%{version}}
%{!?ruby_sitearch: %global ruby_sitearch %(ruby -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')}

%description
The Mysql2 gem is meant to serve the extremely common use-case of
connecting, querying and iterating on results. Some database libraries out
there serve as direct 1:1 mappings of the already complex C API's
available. This one is not.



%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
%setup -q -c -T
mkdir -p .%{gem_dir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gem_dir} \
            -V \
            --force %{SOURCE0}

# Remove developer files
rm -rf .%{gem_instdir}/.gitignore
rm -rf .%{gem_instdir}/.rspec
rm -rf .%{gem_instdir}/.rvmrc
rm -rf .%{gem_instdir}/.travis.yml
# Remove extra gemspec file
rm -rf .%{gem_instdir}/%{gem_name}.gemspec

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%if 0%{?el6}
# gem_extdir doesn't really work on EL 6
mkdir -p %{buildroot}%{ruby_sitearch}/mysql2
# move the extension to ruby_sitearch
mv %{buildroot}%{gem_instdir}/lib/mysql2/mysql2.so %{buildroot}%{ruby_sitearch}/mysql2
%else
mkdir -p %{buildroot}%{gem_extdir}/lib/mysql2
# move the extension to gem_extdir
mv %{buildroot}%{gem_instdir}/lib/mysql2/mysql2.so %{buildroot}%{gem_extdir}/lib/mysql2
%endif

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{gem_instdir}/ext

%check
pushd .%{gem_instdir}
  # From README.md:
  # The tests require the "test" database to exist, and expect to connect both
  # as root and the running user, both with a blank password:
  # CREATE DATABASE test;
  # CREATE USER '<user>'@'localhost' IDENTIFIED BY '';
  # GRANT ALL PRIVILEGES ON test.* TO '<user>'@'localhost';
  # I'm punting on this for Fedora.
  #rspec -Ilib spec
popd


%files
%dir %{gem_instdir}
%{gem_libdir}
%if 0%{?el6}
%{ruby_sitearch}/%{gem_name}
%else
%{gem_extdir}
%endif
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/MIT-LICENSE
%doc %{gem_instdir}/README.md
%{gem_instdir}/Gemfile
%{gem_instdir}/Rakefile
%{gem_instdir}/benchmark
%{gem_instdir}/examples
%{gem_instdir}/spec
%{gem_instdir}/tasks

%changelog
* Fri Feb 15 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.11-3
- Binary extension moved into ruby_sitearch dir

* Thu Feb 14 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.11-2
- RHEL 6 compatibility

* Sat Aug 04 2012 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.11-1
- Initial package, created by gem2rpm 0.8.1
